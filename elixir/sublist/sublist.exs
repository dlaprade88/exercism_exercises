defmodule Sublist do
  @doc """
  Returns whether the first list is a sublist or a superlist of the second list
  and if not whether it is equal or unequal to the second list.
  """
  def compare(a, b) do
    cond do
      a == b          -> :equal
      sublist?(a,b)   -> :sublist
      superlist?(a,b) -> :superlist
      true            -> :unequal
    end
  end

  defp sublist?(a,b) do
    cond do
      a == b                -> true
      length(a) < length(b) -> do_sublist(a,b,a,b)
      true                  -> false
    end
  end

  defp do_sublist([], _, _, _) do; true; end
  defp do_sublist(a, b, orig_a, orig_b) do
    if hd(a) === hd(b) do
      do_sublist tl(a), tl(b), orig_a, orig_b
    else
      sublist? orig_a, tl(orig_b)
    end
  end

  defp superlist?(a,b) do
    sublist?(b,a)
  end

end
