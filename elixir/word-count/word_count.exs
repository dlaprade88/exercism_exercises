defmodule Words do
  @doc """
  Count the number of words in the sentence.

  Words are compared case-insensitively.
  """
  @spec count(String.t) :: map()

  def count(sentence) do
    words = pull_words_from sentence
    Enum.reduce words, %{}, fn(word, accumulator) ->
      Map.update accumulator, word, 1, &(&1 + 1)
    end
  end

  def pull_words_from(sentence) do
    sentence
    |> String.downcase
    |> String.replace(~r/[^a-zA-Z\d\s-äöüÄÖÜß]/, " ")
    |> String.split
  end

end
