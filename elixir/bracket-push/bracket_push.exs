defmodule BracketPush do
  @doc """
  Checks that all the brackets and braces in the string are matched correctly, and nested correctly
  """
  @spec check_brackets(String.t) :: boolean
  def check_brackets(str) do
    String.codepoints(str)
      |> Enum.filter(fn(x) -> is_bracket?(x) end)
      |> do_check_brackets
  end

  defp do_check_brackets( [], brackets ) do
    Enum.count(brackets) == 0
  end
  defp do_check_brackets( [char|list], brackets \\ []) do
    cond do
      is_open_bracket?(char) -> do_check_brackets(list, [char | brackets])
      brackets == []         -> false
      hd(brackets) == matching_bracket(char) ->
        do_check_brackets(list, tl(brackets))
      true                   -> false
    end
  end

  defp is_bracket?(char) do
    String.match?(char, ~r/\[|\]|\{|\}|\(|\)/)
  end

  defp is_open_bracket?(char) do
    String.match?(char, ~r/\[|\{|\(/)
  end

  defp matching_bracket(char) do
    case char do
      "]" -> "["
      "}" -> "{"
      ")" -> "("
    end
  end

end
