defmodule Anagram do
  @doc """
  Returns all candidates that are anagrams of, but not equal to, 'base'.
  """
  @spec match(String.t, [String.t]) :: [String.t]
  def match(base, candidates) do
    match_fnct = fn(x) -> do_match(base, x) end
    Enum.filter(candidates, match_fnct)
  end

  defp do_match(base, candidate) do
    distinct?(base, candidate) &&
      equal_lengths?(base, candidate) &&
      same_letters?(base, candidate)
  end

  defp distinct?(x, y) do
    !String.match? y, ~r/#{x}/i
  end

  defp equal_lengths?(x, y) do
    String.length(x) == String.length(y)
  end

  defp same_letters?("", "") do; true; end
  defp same_letters?(target, candidate) do
    regex = ~r/#{String.first(candidate)}/i
    if String.match?(target, regex) do
      new_target = String.replace(target, regex, "", global: false)
      new_candidate = String.slice(candidate, 1..-1) # remove first char from string
      same_letters?(new_target, new_candidate)
    else
      false
    end
  end


end
