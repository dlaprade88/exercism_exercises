defmodule RunLengthEncoder do
  @doc """
  Generates a string where consecutive elements are represented as a data value and count.
  "HORSE" => "1H1O1R1S1E"
  For this example, assume all input are strings, that are all uppercase letters.
  It should also be able to reconstruct the data into its original form.
  "1H1O1R1S1E" => "HORSE"
  """
  @spec encode(string) :: String.t
  def encode(string) do
    do_encode String.codepoints(string) # make the string a list
  end

  defp do_encode([], acc) do; acc; end
  defp do_encode([char|tail], accumulator \\ "") do
    [count, remainder] = count_continuous(tail, char)
    do_encode(remainder, accumulator <> to_string(count) <> char)
  end

  defp count_continuous([], _, acc) do; [ acc, [] ]; end
  defp count_continuous(list, char, acc \\ 1) do
    if hd(list) == char do
      count_continuous tl(list), char, acc + 1
    else
      [ acc, list ]
    end
  end

  @spec decode(string) :: String.t
  def decode(string) do
    do_decode String.codepoints(string)
  end

  defp do_decode([], acc) do; acc; end
  defp do_decode(char_list, accumulator \\ "") do;
    [integer, remainder] = pull_integer_off_of( char_list )
    decoded_string = produce_string(integer, hd(remainder))
    do_decode tl(remainder), accumulator<>decoded_string
  end

  defp produce_string(0, _, acc) do; acc; end
  defp produce_string(i, char, acc \\ "") do
    produce_string(i-1, char, acc <> char)
  end

  defp pull_integer_off_of([], acc) do; String.to_integer acc; end
  defp pull_integer_off_of([head|tail], acc \\ "") do
    try do
      String.to_integer head
      pull_integer_off_of(tail, acc <> head)
    rescue
      ArgumentError -> [String.to_integer(acc), [head|tail]]
    end
  end
end
