defmodule Bob do
  def hey(input) do
    # remove numbers and empty spaces
    cleaned_input = String.replace(input, ~r/\d|\ /, "")
    cond do
      String.upcase(cleaned_input) == cleaned_input ->
        "Whoa, chill out!"
      String.last(cleaned_input) == "?" ->
        "Sure."
      true ->
        "Whatever."
    end
  end
end
