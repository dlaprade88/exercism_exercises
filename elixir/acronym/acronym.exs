defmodule Acronym do
  @doc """
  Generate an acronym from a string.
  "This is a string" => "TIAS"
  """
  @spec abbreviate(string) :: String.t()
  def abbreviate(string) do
    String.replace(string, ~r/[^a-zA-Z\d\s]/, " ")  # replace punctuation with spaces
      |> String.split(~r/(?=[A-Z])|\s/, trim: true) # add space before uppercase letters
      |> Enum.map(&(String.first &1))               # get the first letter of each word
      |> Enum.map(&(String.upcase &1))              # make the letters uppercase
      |> Enum.reduce("", &(&2 <> &1))               # concatenate all the letters
  end
end
