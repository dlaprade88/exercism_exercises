defmodule ListOps do
  # Please don't use any external modules (especially List) in your
  # implementation. The point of this exercise is to create these basic functions
  # yourself.
  #
  # Note that `++` is a function from an external module (Kernel, which is
  # automatically imported) and so shouldn't be used either.

  require IEx

  @spec count(list) :: non_neg_integer
  def count(list) do
    ListOps.reduce list, 0, fn(x, acc) -> acc + 1 end
  end

  @spec reverse(list) :: list
  def reverse(l) do
    reduce l, [], fn(member, list) -> [member | list] end
  end

  @spec map(list, (any -> any)) :: list
  def map(l, f) do
    do_map l, [], f
  end

  defp do_map([], accumulator, _) do
    reverse accumulator
  end

  defp do_map(list, accumulator, fnct) do
    do_map tl(list), [fnct.(hd list) | accumulator], fnct
  end

  @spec filter(list, (any -> as_boolean(term))) :: list
  def filter(list, fnct) do
    do_filter list, [], fnct
  end

  defp do_filter([], accumulator, _) do
    reverse accumulator
  end

  defp do_filter(list, accumulator, fnct) do
    if fnct.(hd list) do
      do_filter tl(list), [hd(list) | accumulator], fnct
    else
      do_filter tl(list), accumulator, fnct
    end
  end

  @type acc :: any
  @spec reduce(list, acc, ((any, acc) -> acc)) :: acc
  def reduce(list, accumulator, fnct) do
    do_reduce list, accumulator, fnct
  end

  defp do_reduce([], accumulator, _) do
    accumulator
  end

  defp do_reduce(list, accumulator, fnct) do
    do_reduce tl(list), fnct.( hd(list), accumulator ), fnct
  end

  @spec append(list, list) :: list
  def append(a, b) do
    b = if is_list(b), do: b, else: [b]
    :erlang.++(a,b)
  end

  @spec concat([[any]]) :: [any]
  def concat(list) do
    reverse do_concat list, []
  end

  defp do_concat([], accumulator) do
    accumulator
  end

  defp do_concat(list, accumulator) do
    head = hd list
    if is_list(head) do
      do_concat tl(list), do_concat(head, accumulator)
    else
      do_concat tl(list), [head | accumulator]
    end
  end

end
